//
//  TestEntity.swift
//  MVPOnboarding
//

import Foundation

struct TestEntity: Codable {
    var title: String?
    var image: String?
    
    enum CodingKeys: String, CodingKey {
        case title
        case image
    }
}
