//
//  TestTableViewCell.swift
//  MVPOnboarding
//
//  Created by Maksym Vitovych on 6/24/21.
//

import UIKit

protocol TestTableViewCellProtocol {
    func display(title: String?)
    func display(image: String?)
}

class TestTableViewCell: UITableViewCell {

    @IBOutlet private weak var titleImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupViews()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        reset()
    }
    
    private func setupViews() {}
    private func reset() {}
    
}

extension TestTableViewCell: TestTableViewCellProtocol {
    func display(title: String?) {
        titleLabel.text = title
    }
    
    func display(image: String?) {
        guard let url = URL(string: image ?? "") else { return }
        titleImageView.loadImage(by: url)
    }
}
