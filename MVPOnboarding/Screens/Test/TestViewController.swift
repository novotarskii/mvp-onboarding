//
//  TestViewController.swift
//  MVPOnboarding
//
//  Created by Maksym Vitovych on 6/24/21.
//

import UIKit

protocol TestViewControllerProtocol: class {

}

class TestViewController: UIViewController {
        
    // MARK: - IBOutlets
    @IBOutlet private weak var tableView: UITableView!
    
    // MARK: - Properties
    
    var presenter: TestPresenterProtocol!
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presenter = TestPresenter(view: self)
        
        setupViews()
    }
    
    // MARK: - UI
    
    private func setupViews() {
        setupTableView()
    }
    
    private func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.registerCell(TestTableViewCell.self)
    }
    
    // MARK: - IBActions
    
}

extension TestViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return presenter.getSectionCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.getRowsCountAt(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueCell(TestTableViewCell.self, for: indexPath)
        presenter.configure(cell, at: indexPath)
        return cell
    }
}

extension TestViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectedItemAt(indexPath: indexPath)
    }
}

extension TestViewController: TestViewControllerProtocol {
    
}
