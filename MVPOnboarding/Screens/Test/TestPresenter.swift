//
//  TestPresenter.swift
//  MVPOnboarding
//
//  Created by Maksym Vitovych on 6/24/21.
//

import Foundation

protocol TestPresenterProtocol {
    func configure(_ cell: TestTableViewCellProtocol, at indexPath: IndexPath)
    func getRowsCountAt(section: Int) -> Int
    func getSectionCount() -> Int
    func didSelectedItemAt(indexPath: IndexPath)
}


class TestPresenter: TestPresenterProtocol {
    
    private unowned let view: TestViewControllerProtocol

    private var items: [TestEntity] = []
    
    required init(view: TestViewControllerProtocol) {
        self.view = view
        getData()
    }
    
    func configure(_ cell: TestTableViewCellProtocol, at indexPath: IndexPath) {
        let item = items[indexPath.row]
        
        cell.display(image: item.image)
        cell.display(title: item.title)
    }
    
    func getRowsCountAt(section: Int) -> Int {
        return items.count
    }
    
    func getSectionCount() -> Int {
        return 1
    }
    
    func didSelectedItemAt(indexPath: IndexPath) {
        // Selection
    }
    
    private func getData() {
        // Request for gettings items [TestEntity] for table view. Look how it works on your project
    }
}
